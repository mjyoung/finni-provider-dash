import { SignIn } from "@clerk/nextjs";

import PublicLayout from "~/layouts/PublicLayout";

export default function SignInPage() {
  return (
    <PublicLayout>
      <main className="container flex flex-1 items-center justify-center">
        <SignIn />
      </main>
    </PublicLayout>
  );
}
