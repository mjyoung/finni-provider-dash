import { ClerkProvider } from "@clerk/nextjs";
import { NextUIProvider } from "@nextui-org/react";
import { type AppType } from "next/app";
import Head from "next/head";
import { Toaster } from "react-hot-toast";

import { api } from "~/utils/api";
import { FINNI_FAVICON_URL } from "~/utils/constants";
import { inter } from "~/utils/fonts";

import "~/styles/globals.css";

const MyApp: AppType = ({ Component, pageProps }) => {
  return (
    <>
      <Head>
        <link
          href={FINNI_FAVICON_URL}
          rel="shortcut icon"
          type="image/x-icon"
        />
      </Head>
      <ClerkProvider
        signInUrl="/login"
        signUpUrl="/signup"
        afterSignInUrl={"/auth/success"}
        afterSignUpUrl={"/auth/success"}
      >
        <main className={`font-sans ${inter.variable}`}>
          <NextUIProvider>
            <Toaster position="top-center" />
            <Component {...pageProps} />
          </NextUIProvider>
        </main>
      </ClerkProvider>
    </>
  );
};

export default api.withTRPC(MyApp);
