import { Tabs, Tab, Card } from "@nextui-org/react";

import CustomPatientFields from "~/features/settings/components/CustomPatientFields";
import ProtectedLayout from "~/layouts/ProtectedLayout";

export default function SettingsPage() {
  return (
    <ProtectedLayout>
      <main className="container my-6 flex flex-1 flex-col gap-8">
        <h1 className="text-2xl">Settings</h1>
        <section>
          <Tabs aria-label="Options" variant="underlined">
            <Tab key="customPatientFields" title="Custom Patient Fields">
              <Card className="p-6">
                <CustomPatientFields />
              </Card>
            </Tab>
          </Tabs>
        </section>
      </main>
    </ProtectedLayout>
  );
}
