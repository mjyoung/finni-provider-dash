import { Breadcrumbs, BreadcrumbItem } from "@nextui-org/react";
import router from "next/router";

import PatientForm from "~/features/patients/components/PatientForm";
import ProtectedLayout from "~/layouts/ProtectedLayout";
import { api } from "~/utils/api";

export default function NewPatientPage() {
  const { data: providerData } = api.provider.getProvider.useQuery(undefined, {
    refetchOnWindowFocus: false,
  });
  const customProviderFields =
    providerData?.provider.customProviderPatientFields ?? [];

  return (
    <ProtectedLayout>
      <main className="container my-6 flex-1">
        <div className="flex flex-col gap-8">
          <h1 className="text-2xl">New Patient</h1>

          <Breadcrumbs>
            <BreadcrumbItem onClick={() => router.push("/patients")}>
              Patients
            </BreadcrumbItem>
            <BreadcrumbItem>New Patient</BreadcrumbItem>
          </Breadcrumbs>

          <PatientForm
            customProviderPatientFields={customProviderFields}
            isReadOnly={false}
          />
        </div>
      </main>
    </ProtectedLayout>
  );
}
