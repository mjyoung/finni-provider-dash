import {
  Button,
  Card,
  Chip,
  Input,
  Select,
  SelectItem,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
} from "@nextui-org/react";
import {
  PatientStatus,
  type Patient,
  type PatientAddress,
} from "@prisma/client";
import clsx from "clsx";
import { debounce } from "lodash";
import { SearchIcon, UserRoundPlusIcon } from "lucide-react";
import { useRouter } from "next/router";
import { useCallback, useState } from "react";

import ProtectedLayout from "~/layouts/ProtectedLayout";
import { api } from "~/utils/api";
import { inter } from "~/utils/fonts";

type GetAllPatientsInput = Parameters<
  (typeof api.patient.getAllPatients)["useQuery"]
>[0];

type GetAllPatientsItem = Patient & {
  addresses: PatientAddress[];
  age: number | null;
};

export default function PatientsPage() {
  const router = useRouter();

  const [queryInput, setQueryInput] = useState<GetAllPatientsInput>({
    sortBy: "name",
    sortDirection: "asc",
  });

  const { data, isLoading } = api.patient.getAllPatients.useQuery(queryInput, {
    refetchOnWindowFocus: false,
    keepPreviousData: true,
  });

  const debouncedSetQueryInput = useCallback(
    debounce((nextValue: GetAllPatientsInput) => setQueryInput(nextValue), 300),
    [],
  );

  const renderCell = useCallback(
    (
      patient: Patient & { addresses: PatientAddress[]; age: number | null },
      columnKey: string,
    ) => {
      if (!patient) return null;
      switch (columnKey) {
        case "name": {
          return `${patient.firstName} ${patient.lastName}`;
        }
        case "status": {
          return (
            <Chip
              className={clsx({
                "bg-green-300": patient.status === PatientStatus.Active,
                "bg-yellow-300": patient.status === PatientStatus.Inquiry,
                "bg-blue-300": patient.status === PatientStatus.Onboarding,
                "bg-red-300": patient.status === PatientStatus.Churned,
              })}
            >
              {patient.status}
            </Chip>
          );
        }
        case "age": {
          return patient.age;
        }
        case "location": {
          return (
            <div className="flex flex-col gap-2">
              {patient.addresses.map((address) => (
                <p key={address.id}>
                  {address.city}, {address.state}
                </p>
              ))}
            </div>
          );
        }
        default:
          return null;
      }
    },
    [],
  );

  return (
    <ProtectedLayout>
      <main className="container my-6 flex-1">
        <div className="flex flex-col gap-8">
          <div className="flex items-center gap-4">
            <h1 className="text-2xl">Patients</h1>
            <Button
              color="primary"
              startContent={<UserRoundPlusIcon className="h-5 w-5" />}
              onClick={() => router.push("/patients/new")}
            >
              Add new patient
            </Button>
          </div>
          <Card className="flex flex-col gap-4 p-4">
            <div className="flex items-center gap-4">
              <Input
                className="max-w-60"
                label="Search by Name"
                startContent={<SearchIcon className="h-4 w-4" />}
                onValueChange={(value) => {
                  debouncedSetQueryInput({ ...queryInput, filterName: value });
                }}
              />
              <Select
                className="max-w-40"
                classNames={{ listbox: `${inter.className}` }} // necessary because the dropdown renders as a portal
                label="Status"
                selectionMode="single"
                selectedKeys={
                  queryInput.filterStatus ? [queryInput.filterStatus] : []
                }
                onChange={(e) => {
                  debouncedSetQueryInput({
                    ...queryInput,
                    filterStatus:
                      (e.target.value as PatientStatus) || undefined,
                  });
                }}
              >
                {Object.values(PatientStatus).map((status) => (
                  <SelectItem key={status} value={status}>
                    {status}
                  </SelectItem>
                ))}
              </Select>
              <Input
                className="max-w-60"
                label="Search by Location"
                startContent={<SearchIcon className="h-4 w-4" />}
                onValueChange={(value) => {
                  debouncedSetQueryInput({
                    ...queryInput,
                    filterLocation: value,
                  });
                }}
              />
            </div>
          </Card>
          <Table
            classNames={{ wrapper: clsx({ "animate-pulse": isLoading }) }}
            aria-label="Patients Table"
            isStriped
            onSortChange={(descriptor) => {
              setQueryInput((prev) => ({
                ...prev,
                sortBy: descriptor.column as GetAllPatientsInput["sortBy"],
                sortDirection:
                  descriptor.direction === "ascending" ? "asc" : "desc",
              }));
            }}
            sortDescriptor={{
              column: queryInput.sortBy,
              direction:
                queryInput.sortDirection === "asc" ? "ascending" : "descending",
            }}
          >
            <TableHeader>
              <TableColumn key="name" allowsSorting>
                Name
              </TableColumn>
              <TableColumn key="status" allowsSorting>
                Status
              </TableColumn>
              <TableColumn key="age" allowsSorting>
                Age
              </TableColumn>
              <TableColumn key="location">Location</TableColumn>
            </TableHeader>
            <TableBody id="table-body" items={data ?? []} isLoading={isLoading}>
              {(item) => (
                <TableRow
                  key={item.id}
                  className="animate-in slide-in-from-top cursor-pointer duration-300"
                  onClick={() => {
                    router.push(`/patients/${item.id}`);
                  }}
                >
                  {(columnKey) => (
                    <TableCell className="max-h-14">
                      {renderCell(
                        item as GetAllPatientsItem,
                        columnKey as string,
                      )}
                    </TableCell>
                  )}
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
      </main>
    </ProtectedLayout>
  );
}
