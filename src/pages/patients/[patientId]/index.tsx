import { BreadcrumbItem, Breadcrumbs, Button } from "@nextui-org/react";
import clsx from "clsx";
import { isUndefined } from "lodash";
import { EditIcon } from "lucide-react";
import { useRouter } from "next/router";

import PatientForm from "~/features/patients/components/PatientForm";
import ProtectedLayout from "~/layouts/ProtectedLayout";
import { api } from "~/utils/api";

export default function PatientDetailPage() {
  const router = useRouter();
  const patientId = router.query.patientId as string;

  const { data: providerData, isLoading: isLoadingProviderData } =
    api.provider.getProvider.useQuery(undefined, {
      refetchOnWindowFocus: false,
    });
  const customProviderFields =
    providerData?.provider.customProviderPatientFields ?? [];

  const { data: patientData, isLoading: isLoadingPatientData } =
    api.patient.getPatient.useQuery(
      { id: patientId },
      { enabled: !isUndefined(patientId), refetchOnWindowFocus: false },
    );

  return (
    <ProtectedLayout>
      <main className="container my-6 flex-1">
        <div className="flex flex-col gap-8">
          <div className="flex items-center gap-4">
            <h1 className="text-2xl">Patient Details</h1>
            <Button
              color="primary"
              startContent={<EditIcon className="h-5 w-5" />}
              onClick={() => router.push(`/patients/${patientId}/edit`)}
            >
              Edit
            </Button>
          </div>

          <Breadcrumbs>
            <BreadcrumbItem onClick={() => router.push("/patients")}>
              Patients
            </BreadcrumbItem>
            <BreadcrumbItem>Patients Details</BreadcrumbItem>
          </Breadcrumbs>

          <div
            className={clsx({
              "animate-pulse": isLoadingPatientData || isLoadingProviderData,
            })}
          >
            <PatientForm
              customProviderPatientFields={customProviderFields}
              isReadOnly={true}
              patient={patientData}
            />
          </div>
        </div>
      </main>
    </ProtectedLayout>
  );
}
