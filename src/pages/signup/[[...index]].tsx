import { SignUp } from "@clerk/nextjs";

import PublicLayout from "~/layouts/PublicLayout";

export default function SignUpPage() {
  return (
    <PublicLayout>
      <main className="container flex flex-1 items-center justify-center">
        <SignUp />
      </main>
    </PublicLayout>
  );
}
