import { clerkClient, getAuth } from "@clerk/nextjs/server";
import { type GetServerSideProps } from "next";

import { db } from "~/server/db";

export default function SignUpSuccess() {
  return null;
}

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { userId } = getAuth(ctx.req);
  if (!userId) {
    return {
      redirect: {
        destination: "/login",
        permanent: false,
      },
    };
  }

  const user = await clerkClient.users.getUser(userId);

  const existingUser = await db.user.findUnique({
    where: {
      externalUserId: user.id,
    },
  });

  if (!existingUser) {
    await db.user.create({
      data: {
        externalUserId: user.id,
        provider: {
          create: {
            firstName: user.firstName ?? "",
            lastName: user.lastName ?? "",
          },
        },
      },
    });
  }

  return {
    redirect: {
      destination: "/patients",
      permanent: false,
    },
  };
};
