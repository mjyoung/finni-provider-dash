import { FieldType } from "@prisma/client";
import { z } from "zod";

import { createTRPCRouter, protectedProcedure } from "~/server/api/trpc";
import { db } from "~/server/db";

export const providerRouter = createTRPCRouter({
  getProvider: protectedProcedure.query(async ({ ctx }) => {
    const provider = await db.provider.findFirst({
      where: {
        user: {
          externalUserId: ctx.auth.userId,
        },
      },
      include: {
        customProviderPatientFields: {
          orderBy: [{ order: "asc" }, { createdAt: "asc" }],
        },
        user: true,
      },
    });

    if (!provider) {
      throw new Error("Provider not found");
    }

    return {
      provider,
    };
  }),
  createCustomProviderPatientField: protectedProcedure
    .input(
      z.object({
        title: z.string(),
        description: z.string().optional(),
        fieldType: z.nativeEnum(FieldType),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const provider = await db.provider.findFirstOrThrow({
        where: {
          user: {
            externalUserId: ctx.auth.userId,
          },
        },
      });
      return db.customProviderPatientField.create({
        data: {
          providerId: provider.id,
          title: input.title,
          description: input.description,
          fieldType: input.fieldType,
        },
      });
    }),

  updateCustomProviderPatientField: protectedProcedure
    .input(
      z.object({
        id: z.string(),
        title: z.string(),
        description: z.string().optional(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      return db.customProviderPatientField.update({
        where: {
          id: input.id,
          provider: {
            user: {
              externalUserId: ctx.auth.userId,
            },
          },
        },
        data: {
          title: input.title,
          description: input.description,
        },
      });
    }),

  deleteCustomProviderPatientField: protectedProcedure
    .input(
      z.object({
        id: z.string(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      return db.customProviderPatientField.delete({
        where: {
          id: input.id,
          provider: {
            user: {
              externalUserId: ctx.auth.userId,
            },
          },
        },
      });
    }),
});
