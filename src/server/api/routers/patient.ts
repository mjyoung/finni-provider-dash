import { FieldType, PatientStatus, type PrismaPromise } from "@prisma/client";
import { differenceInYears } from "date-fns";
import { z } from "zod";

import { fieldTypeToDBColumn } from "~/features/patients/utils";
import { createTRPCRouter, protectedProcedure } from "~/server/api/trpc";
import { db } from "~/server/db";

export const patientRouter = createTRPCRouter({
  getAllPatients: protectedProcedure
    .input(
      z.object({
        sortBy: z.enum(["name", "age", "status"]).optional(),
        sortDirection: z.enum(["asc", "desc"]).optional(),
        filterName: z.string().optional(),
        filterLocation: z.string().optional(),
        filterStatus: z.nativeEnum(PatientStatus).optional(),
      }),
    )
    .query(async ({ ctx, input }) => {
      type PatientFindManyOptions = Parameters<typeof db.patient.findMany>[0];
      const findManyOptions: PatientFindManyOptions = {
        where: {
          providers: {
            some: {
              user: {
                externalUserId: ctx.auth.userId,
              },
            },
          },
        },
        include: {
          addresses: true,
        },
      };

      if (input.sortBy) {
        let sortKey: string = input.sortBy;
        let sortDirection = input.sortDirection;
        if (input.sortBy === "name") {
          sortKey = "firstName";
        }
        if (input.sortBy === "age") {
          sortKey = "dateOfBirth";
          sortDirection = input.sortDirection === "asc" ? "desc" : "asc";
        }
        findManyOptions.orderBy = {
          [sortKey]: sortDirection,
        };
      }

      if (input.filterName) {
        findManyOptions.where = {
          ...findManyOptions.where,
          OR: [
            {
              firstName: {
                contains: input.filterName,
                mode: "insensitive",
              },
            },
            {
              lastName: {
                contains: input.filterName,
                mode: "insensitive",
              },
            },
          ],
        };
      }

      if (input.filterLocation) {
        findManyOptions.where = {
          ...findManyOptions.where,
          addresses: {
            some: {
              OR: [
                {
                  city: {
                    contains: input.filterLocation,
                    mode: "insensitive",
                  },
                },
                {
                  state: {
                    contains: input.filterLocation,
                    mode: "insensitive",
                  },
                },
              ],
            },
          },
        };
      }

      if (input.filterStatus) {
        findManyOptions.where = {
          ...findManyOptions.where,
          status: input.filterStatus,
        };
      }

      const patients = await db.patient.findMany(findManyOptions);
      const formattedPatients = patients.map((patient) => ({
        ...patient,
        age: patient.dateOfBirth
          ? differenceInYears(new Date(), new Date(patient.dateOfBirth))
          : null,
      }));
      return formattedPatients;
    }),
  getPatient: protectedProcedure
    .input(
      z.object({
        id: z.string(),
      }),
    )
    .query(async ({ ctx, input }) => {
      return db.patient.findFirstOrThrow({
        where: {
          id: input.id,
          providers: {
            some: {
              user: {
                externalUserId: ctx.auth.userId,
              },
            },
          },
        },
        include: {
          addresses: true,
          customPatientFields: {
            include: {
              customProviderPatientField: true,
            },
          },
        },
      });
    }),
  createPatient: protectedProcedure
    .input(
      z.object({
        firstName: z.string(),
        lastName: z.string(),
        dateOfBirth: z.date().optional(),
        email: z.string().optional(),
        status: z.nativeEnum(PatientStatus),
        customPatientFields: z.array(
          z.object({
            customFieldId: z.string(),
            customFieldType: z.nativeEnum(FieldType),
            value: z
              .union([z.string(), z.number(), z.boolean(), z.date(), z.null()])
              .optional(),
          }),
        ),
        addresses: z.array(
          z.object({
            street: z.string(),
            city: z.string(),
            state: z.string(),
            zip: z.string(),
          }),
        ),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const provider = await db.provider.findFirstOrThrow({
        where: {
          user: {
            externalUserId: ctx.auth.userId,
          },
        },
      });

      return db.patient.create({
        data: {
          firstName: input.firstName,
          lastName: input.lastName,
          dateOfBirth: input.dateOfBirth ?? null,
          email: input.email,
          status: input.status,
          customPatientFields: {
            createMany: {
              data: input.customPatientFields.map((field) => {
                let value = field.value;
                if (field.customFieldType === FieldType.Number) {
                  value = field.value ? Number(value) : null;
                }
                if (field.customFieldType === FieldType.Date) {
                  value = field.value ? new Date(value as string) : null;
                }
                return {
                  customProviderPatientFieldId: field.customFieldId,
                  [fieldTypeToDBColumn(field.customFieldType)]: value,
                };
              }),
            },
          },
          addresses: {
            createMany: {
              data: input.addresses.map((address) => ({
                zip: address.zip,
                city: address.city,
                state: address.state,
                street: address.street,
              })),
            },
          },
          providers: {
            connect: {
              id: provider.id,
            },
          },
        },
      });
    }),
  updatePatient: protectedProcedure
    .input(
      z.object({
        id: z.string(),
        firstName: z.string(),
        lastName: z.string(),
        dateOfBirth: z.date().optional(),
        email: z.string().optional(),
        status: z.nativeEnum(PatientStatus),
        customPatientFields: z.array(
          z.object({
            customFieldId: z.string(),
            customFieldType: z.nativeEnum(FieldType),
            value: z
              .union([z.string(), z.number(), z.boolean(), z.date(), z.null()])
              .optional(),
          }),
        ),
        addresses: z.array(
          z.object({
            id: z.string(),
            street: z.string(),
            city: z.string(),
            state: z.string(),
            zip: z.string(),
          }),
        ),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      const promises: PrismaPromise<any>[] = [
        db.patient.update({
          where: {
            id: input.id,
            providers: {
              some: {
                user: {
                  externalUserId: ctx.auth.userId,
                },
              },
            },
          },
          data: {
            firstName: input.firstName,
            lastName: input.lastName,
            dateOfBirth: input.dateOfBirth ?? null,
            email: input.email,
            status: input.status,
          },
        }),
      ];

      // Edit custom fields
      input.customPatientFields.forEach((field) => {
        let value = field.value;
        if (field.customFieldType === FieldType.Number) {
          value = field.value ? Number(value) : null;
        }
        if (field.customFieldType === FieldType.Date) {
          value = field.value ? new Date(value as string) : null;
        }

        promises.push(
          db.customPatientField.upsert({
            where: {
              customProviderPatientFieldId_patientId: {
                customProviderPatientFieldId: field.customFieldId,
                patientId: input.id,
              },
            },
            create: {
              customProviderPatientFieldId: field.customFieldId,
              patientId: input.id,
              [fieldTypeToDBColumn(field.customFieldType)]: value,
            },
            update: {
              [fieldTypeToDBColumn(field.customFieldType)]: value,
            },
          }),
        );
      });

      // Delete any addresses that have been removed
      const inputAddressIds = input.addresses.map((address) => address.id);
      promises.push(
        db.patientAddress.deleteMany({
          where: {
            patientId: input.id,
            NOT: {
              id: {
                in: inputAddressIds,
              },
            },
            patient: {
              providers: {
                some: {
                  user: {
                    externalUserId: ctx.auth.userId,
                  },
                },
              },
            },
          },
        }),
      );

      // Create or update addresses
      input.addresses.forEach((address) => {
        if (address.id.startsWith("new")) {
          promises.push(
            db.patientAddress.create({
              data: {
                zip: address.zip,
                city: address.city,
                state: address.state,
                street: address.street,
                patient: {
                  connect: {
                    id: input.id,
                  },
                },
              },
            }),
          );
        } else {
          promises.push(
            db.patientAddress.update({
              where: {
                id: address.id,
              },
              data: {
                zip: address.zip,
                city: address.city,
                state: address.state,
                street: address.street,
              },
            }),
          );
        }
      });

      await db.$transaction(promises);

      const patient = await db.patient.findUniqueOrThrow({
        where: {
          id: input.id,
        },
      });
      return patient;
    }),
  deletePatient: protectedProcedure
    .input(
      z.object({
        id: z.string(),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      return db.patient.delete({
        where: {
          id: input.id,
          providers: {
            some: {
              user: {
                externalUserId: ctx.auth.userId,
              },
            },
          },
        },
      });
    }),
});
