import { FieldType } from "@prisma/client";

export type PatientFieldValueColumn =
  | "booleanValue"
  | "dateValue"
  | "numberValue"
  | "textValue";

export const fieldTypeToDBColumn = (
  customFieldType: FieldType,
): PatientFieldValueColumn => {
  switch (customFieldType) {
    case FieldType.Boolean:
      return "booleanValue";
    case FieldType.Date:
      return "dateValue";
    case FieldType.Number:
      return "numberValue";
    case FieldType.TextLong:
    case FieldType.TextShort:
      return "textValue";
  }
};
