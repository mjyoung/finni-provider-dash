import {
  Button,
  Card,
  Checkbox,
  Divider,
  Input,
  Select,
  SelectItem,
  Textarea,
} from "@nextui-org/react";
import {
  FieldType,
  PatientStatus,
  type CustomProviderPatientField,
} from "@prisma/client";
import { type inferRouterOutputs } from "@trpc/server";
import { format } from "date-fns";
import { isEmpty } from "lodash";
import { useRouter } from "next/router";
import { useCallback, useEffect } from "react";
import { useFieldArray, useForm } from "react-hook-form";
import toast from "react-hot-toast";

import { fieldTypeToDBColumn } from "~/features/patients/utils";
import { type AppRouter } from "~/server/api/root";
import { api } from "~/utils/api";
import { inter } from "~/utils/fonts";
import { US_STATES_MAP } from "~/utils/geolocation";

interface Props {
  customProviderPatientFields: CustomProviderPatientField[];
  patient?: inferRouterOutputs<AppRouter>["patient"]["getPatient"];
  isReadOnly?: boolean;
}

export default function PatientForm({
  patient,
  customProviderPatientFields = [],
  isReadOnly = false,
}: Props) {
  const router = useRouter();

  const getDefaultProviderFieldValues = useCallback(() => {
    return customProviderPatientFields.map((field) => ({
      customFieldId: field.id,
      customFieldType: field.fieldType,
      title: field.title,
      description: field.description,
      value: undefined,
    }));
  }, [customProviderPatientFields]);

  const getCustomPatientFieldValues = useCallback(() => {
    return customProviderPatientFields.map((field) => {
      const patientField = patient?.customPatientFields?.find(
        (f) => f.customProviderPatientField.id === field.id,
      );
      return {
        customFieldId: field.id,
        customFieldType: field.fieldType,
        title: field.title,
        description: field.description,
        value: patientField
          ? patientField[fieldTypeToDBColumn(field.fieldType)]
          : undefined,
      };
    });
  }, [customProviderPatientFields, patient?.customPatientFields]);

  const { isLoading: isLoadingCreatePatient, mutate: createPatientMutation } =
    api.patient.createPatient.useMutation({
      onSuccess: (data) => {
        toast.success("Patient created successfully");
        const patientId = data.id;
        router.push(`/patients/${patientId}`);
      },
    });

  const { isLoading: isLoadingUpdatePatient, mutate: updatePatientMutation } =
    api.patient.updatePatient.useMutation({
      onSuccess: (_data) => {
        toast.success("Patient updated successfully");
      },
    });

  const { isLoading: isLoadingDeletePatient, mutate: deletePatientMutation } =
    api.patient.deletePatient.useMutation({
      onSuccess: (_data) => {
        toast.success("Patient deleted successfully");
        router.push("/patients");
      },
    });

  const { register, getValues, setValue, control, watch } = useForm({
    defaultValues: {
      firstName: patient?.firstName ?? "",
      lastName: patient?.lastName ?? "",
      dateOfBirth: patient?.dateOfBirth?.toISOString() ?? null,
      email: patient?.email ?? "",
      status: patient?.status ?? null,
      addresses: patient?.addresses ?? [],
      customPatientFields: isEmpty(patient?.customPatientFields)
        ? getDefaultProviderFieldValues()
        : getCustomPatientFieldValues(),
    },
  });

  const { fields: customFields } = useFieldArray({
    name: "customPatientFields",
    control,
    keyName: "fieldArrayCustomFieldId",
  });

  const {
    fields: addresses,
    append,
    remove,
  } = useFieldArray({
    name: "addresses",
    control,
    keyName: "fieldArrayAddressId",
  });

  const watchAllFields = watch();
  useEffect(() => {
    if (patient) {
      setValue("firstName", patient.firstName);
      setValue("lastName", patient.lastName);
      setValue(
        "dateOfBirth",
        patient.dateOfBirth ? format(patient.dateOfBirth, "yyyy-MM-dd") : null,
      );
      setValue("email", patient.email ?? "");
      setValue("status", patient.status ?? null);
      setValue("addresses", patient.addresses ?? []);
    }
  }, [patient, setValue]);

  useEffect(() => {
    if (!isEmpty(customProviderPatientFields)) {
      if (isEmpty(patient?.customPatientFields)) {
        setValue("customPatientFields", getDefaultProviderFieldValues());
      } else {
        setValue("customPatientFields", getCustomPatientFieldValues());
      }
    }
  }, [patient?.customPatientFields, customProviderPatientFields, setValue]);

  const handleCreate = () => {
    const values = getValues();
    createPatientMutation({
      firstName: values.firstName,
      lastName: values.lastName,
      dateOfBirth: values.dateOfBirth
        ? new Date(`${values.dateOfBirth}T12:00:00`)
        : undefined,
      email: values.email,
      status: values.status ?? PatientStatus.Inquiry,
      customPatientFields: values.customPatientFields.map((field) => ({
        ...field,
        value:
          field.customFieldType === FieldType.Date
            ? new Date(`${field.value as string}T12:00:00`)
            : field.value,
      })),
      addresses: values.addresses,
    });
  };

  const handleUpdate = () => {
    const values = getValues();
    if (patient?.id) {
      updatePatientMutation({
        id: patient.id,
        firstName: values.firstName,
        lastName: values.lastName,
        dateOfBirth: values.dateOfBirth
          ? new Date(`${values.dateOfBirth}T12:00:00`)
          : undefined,
        email: values.email,
        status: values.status ?? PatientStatus.Inquiry,
        customPatientFields: values.customPatientFields.map((field) => ({
          ...field,
          value:
            field.customFieldType === FieldType.Date
              ? new Date(`${field.value as string}T12:00:00`)
              : field.value,
        })),
        addresses: values.addresses,
      });
    }
  };

  const handleDelete = () => {
    if (patient?.id) {
      deletePatientMutation({ id: patient.id });
    }
  };

  return (
    <div className="mb-20 flex flex-col gap-8">
      {/* PRIMARY INFORMATION */}
      <Card className="flex flex-col gap-4 p-6">
        <h1 className="text-xl">Primary Information</h1>
        <div className="grid max-w-lg grid-cols-2 gap-4">
          <Input
            label="First Name"
            isRequired={true}
            isReadOnly={isReadOnly}
            {...register(`firstName`)}
            value={watchAllFields.firstName}
          />
          <Input
            label="Last Name"
            isRequired={true}
            isReadOnly={isReadOnly}
            {...register(`lastName`)}
            value={watchAllFields.lastName}
          />
          <Input
            type="date"
            label="Date of Birth"
            isReadOnly={isReadOnly}
            {...register(`dateOfBirth`)}
            value={watchAllFields.dateOfBirth ?? undefined}
          />
          {isReadOnly ? (
            <Input
              label="Status"
              isReadOnly={isReadOnly}
              {...register(`status`)}
              value={watchAllFields.status ?? ""}
            />
          ) : (
            <Select
              classNames={{ listbox: `${inter.className}` }} // necessary because the dropdown renders as a portal
              label="Status"
              selectionMode="single"
              isRequired={true}
              disabled={isReadOnly}
              {...register(`status`)}
              selectedKeys={
                watchAllFields.status ? [watchAllFields.status] : []
              }
            >
              {Object.values(PatientStatus).map((status) => (
                <SelectItem key={status} value={status}>
                  {status}
                </SelectItem>
              ))}
            </Select>
          )}
          <Input
            label="Email"
            isReadOnly={isReadOnly}
            {...register(`email`)}
            value={watchAllFields.email}
          />
        </div>
      </Card>

      {/* ADDRESS INFORMATION */}
      <Card className="flex flex-col gap-4 p-6">
        <div className="flex items-center gap-4">
          <h1 className="text-xl">Address Information</h1>
          {!isReadOnly && (
            <Button
              className="max-w-40"
              color="primary"
              onClick={() => {
                append({
                  street: "",
                  city: "",
                  state: "",
                  zip: "",
                  id: `new-${addresses.length}`,
                  createdAt: new Date(),
                  updatedAt: new Date(),
                  patientId: patient?.id ?? "",
                });
              }}
            >
              New Address
            </Button>
          )}
        </div>
        <div className="grid max-w-lg gap-6">
          {addresses.map((field, index) => {
            return (
              <div
                key={field.fieldArrayAddressId}
                className="flex flex-col gap-4"
              >
                <Input
                  label={"Street"}
                  placeholder="123 Main St"
                  isReadOnly={isReadOnly}
                  {...register(`addresses.${index}.street`)}
                  value={watchAllFields.addresses?.[index]?.street ?? ""}
                />
                <div className="flex items-center gap-4">
                  <Input
                    label={"City"}
                    isReadOnly={isReadOnly}
                    {...register(`addresses.${index}.city`)}
                    value={watchAllFields.addresses?.[index]?.city ?? ""}
                  />
                  {isReadOnly ? (
                    <Input
                      label="State"
                      isReadOnly={isReadOnly}
                      {...register(`addresses.${index}.state`)}
                      value={watchAllFields.status ?? ""}
                    />
                  ) : (
                    <Select
                      classNames={{ listbox: `${inter.className}` }} // necessary because the dropdown renders as a portal
                      label="State"
                      selectionMode="single"
                      disabled={isReadOnly}
                      {...register(`addresses.${index}.state`)}
                      selectedKeys={
                        watchAllFields.addresses?.[index]?.state
                          ? [watchAllFields.addresses?.[index]?.state ?? ""]
                          : []
                      }
                    >
                      {Object.keys(US_STATES_MAP).map((key) => (
                        <SelectItem key={key} value={key}>
                          {key}
                        </SelectItem>
                      ))}
                    </Select>
                  )}
                  <Input
                    label={"Zip Code"}
                    isReadOnly={isReadOnly}
                    {...register(`addresses.${index}.zip`)}
                    value={watchAllFields.addresses?.[index]?.zip ?? ""}
                  />
                </div>
                {!isReadOnly && (
                  <Button
                    color="default"
                    className="max-w-40"
                    onClick={() => remove(index)}
                  >
                    Delete Address
                  </Button>
                )}
              </div>
            );
          })}
        </div>
      </Card>

      {/* CUSTOM FIELDS */}
      <Card className="flex flex-col gap-4 p-6">
        <h1 className="text-xl">Custom Fields</h1>
        <div className="grid max-w-lg gap-6">
          {customFields.map((field, index) => {
            return (
              <div
                className="flex flex-col gap-4"
                key={field.fieldArrayCustomFieldId}
              >
                {field.description}
                {field.customFieldType === "Boolean" && (
                  <Checkbox
                    type="checkbox"
                    isReadOnly={isReadOnly}
                    {...register(`customPatientFields.${index}.value`)}
                    isSelected={Boolean(
                      watchAllFields.customPatientFields?.[index]?.value,
                    )}
                    onValueChange={(value) => {
                      setValue(`customPatientFields.${index}.value`, value);
                    }}
                  >
                    {field.title}
                  </Checkbox>
                )}
                {field.customFieldType === "Number" && (
                  <Input
                    type="number"
                    label={field.title}
                    isReadOnly={isReadOnly}
                    {...register(`customPatientFields.${index}.value`)}
                    value={
                      (watchAllFields.customPatientFields?.[index]
                        ?.value as string) ?? undefined
                    }
                  />
                )}
                {field.customFieldType === "Date" && (
                  <Input
                    type="date"
                    label={field.title}
                    isReadOnly={isReadOnly}
                    {...register(`customPatientFields.${index}.value`)}
                    value={
                      watchAllFields.customPatientFields?.[index]?.value
                        ? format(
                            watchAllFields.customPatientFields?.[index]
                              ?.value as Date,
                            "yyy-MM-dd",
                          )
                        : undefined
                    }
                  />
                )}
                {field.customFieldType === "TextShort" && (
                  <Input
                    type="text"
                    label={field.title}
                    isReadOnly={isReadOnly}
                    {...register(`customPatientFields.${index}.value`)}
                    value={
                      (watchAllFields.customPatientFields?.[index]
                        ?.value as string) ?? ""
                    }
                  />
                )}
                {field.customFieldType === "TextLong" && (
                  <Textarea
                    label={field.title}
                    isReadOnly={isReadOnly}
                    {...register(`customPatientFields.${index}.value`)}
                    value={
                      (watchAllFields.customPatientFields?.[index]
                        ?.value as string) ?? ""
                    }
                  />
                )}
                {index !== customFields.length - 1 && (
                  <Divider className="mb-2 mt-6" />
                )}
              </div>
            );
          })}
        </div>
      </Card>
      {!isReadOnly && (
        <div className="fixed bottom-0 z-10 -ml-6 flex w-[calc(100vw-240px)] justify-between gap-4 border-t border-t-gray-300 bg-white py-2 pl-4 pr-6">
          <div className="flex items-center gap-4">
            {patient && (
              <Button
                color="danger"
                onClick={() => {
                  handleDelete();
                }}
                isLoading={isLoadingDeletePatient}
              >
                Delete Patient
              </Button>
            )}
          </div>

          <div className="flex items-center gap-4">
            <Button
              color="default"
              onClick={() => {
                if (patient?.id) {
                  router.push(`/patients/${patient.id}`);
                } else {
                  router.push("/patients");
                }
              }}
            >
              Cancel
            </Button>
            <Button
              color="primary"
              onClick={() => {
                if (patient) {
                  handleUpdate();
                } else {
                  handleCreate();
                }
              }}
              isLoading={isLoadingCreatePatient || isLoadingUpdatePatient}
            >
              Save
            </Button>
          </div>
        </div>
      )}
    </div>
  );
}
