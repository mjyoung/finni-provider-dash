import { useAutoAnimate } from "@formkit/auto-animate/react";
import {
  Button,
  Divider,
  Modal,
  Tooltip,
  useDisclosure,
} from "@nextui-org/react";
import { type CustomProviderPatientField } from "@prisma/client";
import { EditIcon, TrashIcon } from "lucide-react";
import { useState } from "react";

import { api } from "~/utils/api";
import { inter } from "~/utils/fonts";

import CustomPatientFieldModal from "./CustomPatientFieldModal";

export default function CustomProviderPatientFields() {
  const [selectedField, setSelectedField] =
    useState<CustomProviderPatientField | null>(null);

  const [parent, _enableAnimations] = useAutoAnimate();
  const { isOpen, onOpen, onOpenChange, onClose } = useDisclosure();

  const { data, refetch } = api.provider.getProvider.useQuery(undefined, {});
  const customFields = data?.provider?.customProviderPatientFields ?? [];

  const deleteCustomFieldMutation =
    api.provider.deleteCustomProviderPatientField.useMutation({
      onSettled: () => {
        refetch();
      },
    });

  return (
    <div className="flex flex-col gap-8">
      {customFields.length === 0 && <div>No custom fields set</div>}
      {customFields.length > 0 && (
        <ul className="flex flex-col gap-12 lg:gap-6" ref={parent}>
          {customFields.map((field) => {
            return (
              <li key={field.id} className="flex flex-col gap-6">
                <div className="flex flex-col">
                  <div className="flex flex-row items-center justify-between gap-2">
                    <div className="flex items-center gap-2">
                      <span className="font-semibold">{field.title}</span>
                      <span className="text-sm">({field.fieldType})</span>
                    </div>
                  </div>
                  <p className="mt-1 text-sm">{field.description}</p>
                  <div className="mt-4 flex items-center gap-2">
                    <Tooltip
                      classNames={{ content: `${inter.className}` }}
                      color="foreground"
                      content="Edit Field"
                    >
                      <EditIcon
                        className="h-5 w-5 cursor-pointer text-black/50 hover:text-black"
                        onClick={() => {
                          setSelectedField(field);
                          onOpen();
                        }}
                      />
                    </Tooltip>
                    <Tooltip
                      classNames={{ content: `${inter.className}` }}
                      color="foreground"
                      content="Delete Field"
                    >
                      <TrashIcon
                        className="h-5 w-5 cursor-pointer text-black/50 hover:text-black"
                        onClick={() =>
                          deleteCustomFieldMutation.mutate({ id: field.id })
                        }
                      />
                    </Tooltip>
                  </div>
                </div>
                <Divider />
              </li>
            );
          })}
        </ul>
      )}
      <Button
        className="max-w-60"
        color="primary"
        onClick={() => {
          setSelectedField(null);
          onOpen();
        }}
      >
        Add Field
      </Button>
      <Modal
        classNames={{ wrapper: `${inter.className}` }} // necessary because the dropdown renders as a portal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        onClose={() => {
          setSelectedField(null);
          onClose();
        }}
      >
        <CustomPatientFieldModal
          customField={selectedField}
          onSubmit={async () => {
            await refetch();
          }}
        />
      </Modal>
    </div>
  );
}
