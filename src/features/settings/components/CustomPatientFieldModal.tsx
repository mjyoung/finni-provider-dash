import {
  Button,
  Input,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Select,
  SelectItem,
} from "@nextui-org/react";
import { FieldType, type CustomProviderPatientField } from "@prisma/client";
import { isEmpty } from "lodash";
import { useForm } from "react-hook-form";

import { api } from "~/utils/api";
import { inter } from "~/utils/fonts";

interface Props {
  customField: CustomProviderPatientField | null;
  onSubmit: () => Promise<void>;
}

export default function CustomPatientFieldModal({
  customField,
  onSubmit,
}: Props) {
  const { register, getValues, watch } = useForm({
    defaultValues: {
      title: customField?.title ?? "",
      description: customField?.description ?? "",
      fieldType: customField?.fieldType ?? FieldType.TextShort,
    },
  });

  const watchAllFields = watch();

  const createCustomFieldMutation =
    api.provider.createCustomProviderPatientField.useMutation({
      onSettled: () => {
        onSubmit();
      },
    });
  const updateCustomFieldMutation =
    api.provider.updateCustomProviderPatientField.useMutation({
      onSettled: () => {
        onSubmit();
      },
    });

  return (
    <ModalContent>
      {(onClose) => {
        return (
          <>
            <ModalHeader>
              {customField ? "Edit Custom Field" : "New Custom Field"}
            </ModalHeader>
            <ModalBody>
              <div className="flex flex-col gap-6">
                <Input
                  className=""
                  label="Title"
                  defaultValue={customField?.title ?? ""}
                  isRequired={true}
                  {...register(`title`)}
                />
                <Input
                  className=""
                  label="Description"
                  defaultValue={customField?.description ?? ""}
                  {...register(`description`)}
                />
                <Select
                  classNames={{ listbox: `${inter.className}` }} // necessary because the dropdown renders as a portal
                  label="Field Type"
                  selectionMode="single"
                  defaultSelectedKeys={[
                    customField?.fieldType ?? FieldType.TextShort,
                  ]}
                  isRequired={true}
                  isDisabled={!isEmpty(customField)}
                  {...register(`fieldType`)}
                >
                  {Object.values(FieldType).map((fieldType) => (
                    <SelectItem key={fieldType} value={fieldType}>
                      {fieldType}
                    </SelectItem>
                  ))}
                </Select>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="default"
                onClick={async () => {
                  onClose();
                }}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                onClick={async () => {
                  const values = getValues();
                  if (customField) {
                    await updateCustomFieldMutation.mutateAsync({
                      id: customField.id,
                      title: values.title,
                      description: values.description,
                    });
                  } else {
                    await createCustomFieldMutation.mutateAsync({
                      title: values.title,
                      description: values.description,
                      fieldType: values.fieldType,
                    });
                  }

                  onClose();
                }}
                isDisabled={
                  isEmpty(watchAllFields.title) ||
                  isEmpty(watchAllFields.fieldType)
                }
              >
                Save
              </Button>
            </ModalFooter>
          </>
        );
      }}
    </ModalContent>
  );
}
