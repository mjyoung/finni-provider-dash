export const FINNI_LOGO_URL =
  "https://assets-global.website-files.com/6297d5d89ac9c5b4308579e1/6297d5d89ac9c550828579f0_Logo.svg";

export const FINNI_FAVICON_URL =
  "https://assets-global.website-files.com/6297d5d89ac9c5b4308579e1/62990e8d118ed52cbc0bcf7d_favicon.ico";
