import { SignOutButton } from "@clerk/nextjs";
import clsx from "clsx";
import { LogOutIcon, SettingsIcon, UsersIcon } from "lucide-react";
import Link from "next/link";
import { useRouter } from "next/router";
import { type PropsWithChildren } from "react";

import { FINNI_LOGO_URL } from "~/utils/constants";

function Sidebar() {
  const { pathname } = useRouter();

  const listLinkClassName =
    "flex w-full cursor-pointer items-center gap-4 px-5 py-3 hover:bg-brand-orange/15";
  return (
    <nav className="flex h-screen w-[240px] flex-col items-start overflow-y-auto border-r border-r-gray-300 bg-white">
      <img className="mb-8 ml-5 mt-5 h-8" src={FINNI_LOGO_URL} alt="" />
      <ul className="flex w-full flex-1 flex-col">
        <li>
          <Link
            className={clsx(
              { "text-brand-orange": pathname.startsWith("/patients") },
              listLinkClassName,
            )}
            href="/patients"
          >
            <UsersIcon className="h-5 w-5" />
            <span>Patients</span>
          </Link>
        </li>
      </ul>

      <ul className="flex w-full flex-col">
        <li>
          <Link
            className={clsx(
              { "text-brand-orange": pathname === "/settings" },
              listLinkClassName,
            )}
            href="/settings"
          >
            <SettingsIcon className="h-5 w-5" />
            <span>Settings</span>
          </Link>
        </li>
        <SignOutButton>
          <li>
            <div className={clsx(listLinkClassName)}>
              <LogOutIcon className="h-5 w-5" />
              <span>Sign Out</span>
            </div>
          </li>
        </SignOutButton>
      </ul>
    </nav>
  );
}

export default function ProtectedLayout({ children }: PropsWithChildren) {
  return (
    <main className="flex min-h-screen justify-center bg-gray-100">
      <Sidebar />
      <div className="h-screen flex-1 overflow-y-auto bg-gray-100">
        {children}
      </div>
    </main>
  );
}
