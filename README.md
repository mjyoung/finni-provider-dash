# Michael Young x Finni Provider Dashboard

[https://myoung-finni-provider-dash.vercel.app/](https://myoung-finni-provider-dash.vercel.app/)

## What's the stack?

This is a [T3 Stack](https://create.t3.gg/) project bootstrapped with `create-t3-app`. T3 is a full-stack framework for building web applications with Next.js, Prisma, and tRPC.

### Frontend

- Next.js / React deployed to Vercel
- Tailwind CSS
- NextUI as component library

### Backend

- Postgres using Neon as serverless DB
- Prisma ORM
- tRPC
- Clerk for Auth

## How should I test it?

1. Navigate to the above URL and create an account (sign in with Google or with password).
2. In the sidebar, navigate to [/settings](https://myoung-finni-provider-dash.vercel.app/settings). Here you can configure custom patient fields.
3. In the sidebar, navigate to [/patients](https://myoung-finni-provider-dash.vercel.app/settings) and create a new patient.
4. Test updating and deleting a patient.
5. Test filters and sorts.
