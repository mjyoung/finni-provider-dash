/*
  Warnings:

  - A unique constraint covering the columns `[customProviderPatientFieldId,patientId]` on the table `CustomPatientField` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `patientId` to the `CustomPatientField` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "CustomPatientField" ADD COLUMN     "patientId" TEXT NOT NULL;

-- CreateIndex
CREATE INDEX "CustomPatientField_patientId_idx" ON "CustomPatientField"("patientId");

-- CreateIndex
CREATE UNIQUE INDEX "CustomPatientField_customProviderPatientFieldId_patientId_key" ON "CustomPatientField"("customProviderPatientFieldId", "patientId");

-- CreateIndex
CREATE INDEX "CustomProviderPatientField_providerId_idx" ON "CustomProviderPatientField"("providerId");

-- AddForeignKey
ALTER TABLE "CustomPatientField" ADD CONSTRAINT "CustomPatientField_patientId_fkey" FOREIGN KEY ("patientId") REFERENCES "Patient"("id") ON DELETE CASCADE ON UPDATE CASCADE;
