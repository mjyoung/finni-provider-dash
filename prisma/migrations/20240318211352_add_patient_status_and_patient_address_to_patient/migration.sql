/*
  Warnings:

  - You are about to drop the column `locationCity` on the `Patient` table. All the data in the column will be lost.
  - You are about to drop the column `locationState` on the `Patient` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "PatientStatus" AS ENUM ('Inquiry', 'Onboarding', 'Active', 'Churned');

-- AlterTable
ALTER TABLE "Patient" DROP COLUMN "locationCity",
DROP COLUMN "locationState",
ADD COLUMN     "status" "PatientStatus" NOT NULL DEFAULT 'Inquiry';

-- CreateTable
CREATE TABLE "PatientAddress" (
    "id" TEXT NOT NULL,
    "street" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "state" TEXT NOT NULL,
    "zip" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "patientId" TEXT NOT NULL,

    CONSTRAINT "PatientAddress_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "PatientAddress" ADD CONSTRAINT "PatientAddress_patientId_fkey" FOREIGN KEY ("patientId") REFERENCES "Patient"("id") ON DELETE CASCADE ON UPDATE CASCADE;
