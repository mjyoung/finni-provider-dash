-- CreateEnum
CREATE TYPE "FieldType" AS ENUM ('Boolean', 'Date', 'Number', 'TextLong', 'TextShort');

-- AlterTable
ALTER TABLE "Patient" ADD COLUMN     "dateOfBirth" TIMESTAMP(3),
ADD COLUMN     "email" TEXT,
ADD COLUMN     "locationCity" TEXT,
ADD COLUMN     "locationState" TEXT;

-- CreateTable
CREATE TABLE "CustomProviderPatientField" (
    "id" TEXT NOT NULL,
    "fieldType" "FieldType" NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT,
    "order" INTEGER,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "providerId" TEXT NOT NULL,

    CONSTRAINT "CustomProviderPatientField_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CustomPatientField" (
    "id" TEXT NOT NULL,
    "booleanValue" BOOLEAN,
    "dateValue" TIMESTAMP(3),
    "numberValue" DOUBLE PRECISION,
    "textValue" TEXT,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "customProviderPatientFieldId" TEXT NOT NULL,

    CONSTRAINT "CustomPatientField_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "CustomProviderPatientField" ADD CONSTRAINT "CustomProviderPatientField_providerId_fkey" FOREIGN KEY ("providerId") REFERENCES "Provider"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CustomPatientField" ADD CONSTRAINT "CustomPatientField_customProviderPatientFieldId_fkey" FOREIGN KEY ("customProviderPatientFieldId") REFERENCES "CustomProviderPatientField"("id") ON DELETE CASCADE ON UPDATE CASCADE;
